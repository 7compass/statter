# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'statter/version'

Gem::Specification.new do |spec|
  spec.name          = "statter"
  spec.version       = Statter::VERSION
  spec.authors       = ["Jeff Emminger"]
  spec.email         = ["jeff@7compass.com"]
  spec.summary       = %q{Report stats to the statter backend}
  spec.description   = %q{Report stats to the statter backend.}
  spec.homepage      = "https://bitbucket.org/7compass/statter"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "sucker_punch", "~> 1.2"
  spec.add_runtime_dependency "httparty", "~> 0.13"

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
