require "statter/version"
require "singleton"
require "logger"
require "statter/reporter_job"

module Statter
  
  def config
    yield Config.instance
  end

  def send_counter(name, data)
    agent = Agent.with_config(Config.instance)
    agent.send_data(:counter, name, data)
  end

  def send_value(name, data)
    agent = Agent.with_config(Config.instance)
    agent.send_data(:value, name, data)
  end

  module_function :config
  module_function :send_counter
  module_function :send_value

  class Config
    include Singleton
    attr_accessor :auth_header, :url, :logging_on
  end # Config

  class Agent
    include Singleton
    attr_accessor :config

    def self.logger
      if defined? Rails
        log_path = "log/statter.#{Rails.env}.log"
      else
        log_path = "./statter.log"
      end

      @logger ||= Logger.new(log_path)
    end

    def self.with_config(config)
      i = instance
      i.config = config
      i
    end

    def self.log(str)
      logger.info(str) if Config.instance.logging_on
    end

    def log(str)
      self.class.log(str)
    end

    def send_data(type, name, data)
      log("enqueueing type: #{type}, name: #{name}, data: #{data}")
      ReporterJob.new.async.perform(config, type, name, data)
    end

  end # Agent

end

