
require "httparty"
require "sucker_punch"

class ReporterJob
  include SuckerPunch::Job

  def perform(config, type, name, data)
    Statter::Agent.log("ReporterJob sending type: #{type}, name: #{name}, data: #{data}")

    resp = HTTParty.post(
                "#{config.url}/stats",
                headers: {
                  "x-auth-header" => config.auth_header
                },
                body: { 
                  type: type, 
                  name: name,
                  data: data
                  }
                )
    Statter::Agent.log("sent type: #{type}, name: #{name}, data: #{data}, response: [#{resp.code}] #{resp.headers.inspect}")

  rescue => e
    Statter::Agent.log("Exception sending type: #{type}, name: #{name}, data: #{data}: #{e.message}\n#{e.backtrace.join("\n")}")

  end

end
