# Statter

Send stats to a seven-c-stats backend

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'statter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install statter

## Usage

### Rails

In an initializer:

```ruby
# config/initializers/statter.rb

Statter.config do |config|
  config.auth_header = "your-auth-header-key"
  config.url = "http://url-of-seven-c-stats-backend"
end

```

Then, whereever you want to collect stats:

```ruby
# Send a counter stat
Statter.send_counter "my-counter-stat-name", some_count

# Send a value stat
Statter.send_value "my-value-stat-name", some_value
```

## TODO

* Thread pool?

## Contributing

1. Fork it ( https://bitbucket.org/7compass/statter/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
